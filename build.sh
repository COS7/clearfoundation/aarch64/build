#!/bin/bash

# Ensure we are in our home directory
cd $HOME

# Prepare the environment for building
yum -d0 -y upgrade
yum -d0 -y install gcc-c++ git make rpmdevtools which
rm -f /etc/yum.repos.d/CentOS-Sources.repo

# Install common repository (Makefiles)
git clone https://gitlab.com/clearos/common.git

# Next commands are all that should need to change between packages (parameter #1)
pkg=$(echo "${1:?Must provide package url}" | sed 's,.*/\([^/]*\)\(\.git\)\s*$,\1,')
rm -rf ${pkg}
git clone "${1}"
cd ${pkg}

# Checkout the correct branch and build the srpm and binary rpm packages
git checkout clear7
ln -s ../common/Makefile Makefile
make srpm
yum-builddep -d0 -y *.src.rpm
rpmbuild --define "_topdir $HOME/rpmbuild" --rebuild *.src.rpm
/bin/mv -f *.src.rpm $HOME/rpmbuild/SRPMS/
